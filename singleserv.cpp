// implemnts simple single-threaded http server for unique word counting service
#include <set>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <evhttp.h>

uint16_t const PORT = 5555;
char const * ADDR = "127.0.0.1";

char const * home_page_path = "upload.html";

using std::set;
using std::string;
using std::istringstream;
using std::ostringstream;
using std::ifstream;
using std::cout;

void count_unique(evhttp_request * req);
void send_home_page(evhttp_request * req);
void generic_request_handler(evhttp_request * req, void * arg);
string get_message_body(evbuffer * buf);  //!< \note only 'Content-Type: multipart/form-data' POST messages can be parsed
string read_file(char const * fname);
void dump_request_cb(struct evhttp_request *req, void *arg);


void send_home_page(evhttp_request * req)
{
	evbuffer * buf = evhttp_request_get_output_buffer(req);
	assert(buf && "invalid buffer");

	string home_page_source = read_file(home_page_path);
	evbuffer_add(buf, home_page_source.c_str(), home_page_source.size());

	evhttp_send_reply(req, HTTP_OK, "", buf);
}

void count_unique(evhttp_request * req)
{
	evbuffer * ibuf = evhttp_request_get_input_buffer(req);
	assert(ibuf && "invalid input buffer");
	
	// count uniques
	string body = get_message_body(ibuf);

	set<string> words;
	istringstream in{body};
	string word;
	
	while (in)
	{
		in >> word;
		boost::trim_if(word, !boost::algorithm::is_alpha());
		boost::to_lower(word);
		words.insert(word);
	}
	
	int unique_count = words.size();
	
	// answer
	evbuffer * obuf = evhttp_request_get_output_buffer(req);
	assert(obuf && "invalid output buffer");

	evbuffer_add_printf(obuf, "%d", unique_count);
	evhttp_send_reply(req, HTTP_OK, "", obuf);
}

void generic_request_handler(evhttp_request * req, void * arg)
{
	switch (evhttp_request_get_command(req))
	{
		case EVHTTP_REQ_GET: {
			send_home_page(req);  // TODO: handle /favicon GET request
			break;
		}

		case EVHTTP_REQ_POST: {
			count_unique(req);
			break;
		}

		default:
			dump_request_cb(req, arg);
	}
}

string get_message_body(evbuffer * buf)
{
	string message;
	while (evbuffer_get_length(buf))
	{
		int n;
		char cbuf[128];
		n = evbuffer_remove(buf, cbuf, sizeof(cbuf));
		if (n > 0)
			message += cbuf;
	}

	cout << "info: " << message.size() << " bytes long message handled\n";

	istringstream in{message};

	string start_line;
	getline(in, start_line);
	boost::trim_right(start_line);

	auto bpos = message.find("\r\n\r\n");
	auto epos = message.rfind(start_line);
	assert(bpos != string::npos && epos != string::npos && "logic error: unexpected message format");

	return message.substr(bpos, epos - bpos);
}

string read_file(char const * fname)
{
	ifstream in{fname};
	if (!in.is_open())
		throw std::runtime_error{"unable to open a file"};
	
	ostringstream oss;
	oss << in.rdbuf();
	return oss.str();
}

// copyed from libevent:http-server.c sample
void dump_request_cb(struct evhttp_request *req, void *arg)
{
	const char *cmdtype;
	struct evkeyvalq *headers;
	struct evkeyval *header;
	struct evbuffer *buf;

	switch (evhttp_request_get_command(req)) {
	case EVHTTP_REQ_GET: cmdtype = "GET"; break;
	case EVHTTP_REQ_POST: cmdtype = "POST"; break;
	case EVHTTP_REQ_HEAD: cmdtype = "HEAD"; break;
	case EVHTTP_REQ_PUT: cmdtype = "PUT"; break;
	case EVHTTP_REQ_DELETE: cmdtype = "DELETE"; break;
	case EVHTTP_REQ_OPTIONS: cmdtype = "OPTIONS"; break;
	case EVHTTP_REQ_TRACE: cmdtype = "TRACE"; break;
	case EVHTTP_REQ_CONNECT: cmdtype = "CONNECT"; break;
	case EVHTTP_REQ_PATCH: cmdtype = "PATCH"; break;
	default: cmdtype = "unknown"; break;
	}

	printf("Received a %s request for %s\nHeaders:\n",
		 cmdtype, evhttp_request_get_uri(req));

	headers = evhttp_request_get_input_headers(req);
	for (header = headers->tqh_first; header;
		 header = header->next.tqe_next) {
		printf("  %s: %s\n", header->key, header->value);
	}

	buf = evhttp_request_get_input_buffer(req);
	puts("Input data: <<<");
	while (evbuffer_get_length(buf)) {
		int n;
		char cbuf[128];
		n = evbuffer_remove(buf, cbuf, sizeof(cbuf));
		if (n > 0)
			(void) fwrite(cbuf, 1, n, stdout);
	}
	puts(">>>");
}


int main(int argc, char * argv[])
{
	event_init();
	evhttp * http_server = evhttp_start(ADDR, PORT);
	evhttp_set_gencb(http_server, generic_request_handler, nullptr);

	cout << "server started on " << ADDR << ":" << PORT << std::endl;

	event_dispatch();

	return 0;
}
